const Sequelize=require("sequelize");
const sequelize=new Sequelize('c9','root','p@ss',{dialect:'mysql'});

sequelize.authenticate().then(()=>{
    console.log('Datbase connection succeess');
}).catch(err=>{
    console.log(`Database connection error: ${err}`);
});

class Todo extends Sequelize.Model{};

Todo.init({
   identifier:{
        type:Sequelize.INTEGER,
        primaryKey:true,
        autoIncrement:true
       
   },
   taskName:{
       type:Sequelize.STRING,
       alowNull:false,
       validate:{
           isAlpha:true
           
       }
       
   },
   priority:{
       type:Sequelize.STRING,
       validate:{
       validatePriority(value){
           if(!["LOW","MEDIUM","HIGH"].find(elem=>elem==value))
           {
               throw new Error('Priority should be LOW, MEDIUM or HIGH');
           }
       }
     }
   },
   duration:Sequelize.INTEGER
},
    {
        sequelize,
        modelName:'todos',
        validate:{
            validateTodo(){
                if(this.duration<0){
                    throw new Error('Duration should be a positive integer.');
                }
            }
        }
    }
);

Todo.sync({force:true});
module.exports={
    sequelize,
    Todo
};
