const {sequelize, Todo}=require('./models');
const express=require("express");
const bodyParser=require("body-parser");
const app=express();

app.use(bodyParser.json());


app.get('/todos',(request,response)=>{
    Todo.findAll().then(result=>{
        response.status(200).send(result);
    }).catch(err=>{
        response.status(500).send(err);
    })
});

app.post('/todos',async (request,response)=>{
  
       const todo=request.body;
       if(todo.taskName && todo.duration && todo.priority){
           try{
               const result= await Todo.create(todo);
               response.status(201).send({message:"Todo created successfully!"});
           }
           catch(err){
                response.status(400).send({message:"Todo failed to create"});
            }
       }
       else{
           response.status(400).send({message:"Invalid payload"});
       }
    
   
    
});




app.listen(8080,()=>{
   
   console.log('Server started on port 8080'); 
});

